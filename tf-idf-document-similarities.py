#!/usr/bin/env python

##///////////////////////////////////////////////////////////////////////////////////////////////////
##  TF IDF Feature Extraction Proximity calculator
##
## developed during the MACHINE RESEACH workshop, oct 2016
##//////////////////////////////////////////////////////////////////////////////////////////////////

from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from argparse import ArgumentParser
import sys, os
import numpy as np
from ntpath import basename

ap = ArgumentParser("""
This is a script that estimates the similatity between documents that are taken two by two from a larger collection. The algorithm calculates the average frequencies of the words used within the collection of all the documents, and compares these values with the local frequencies of the words within each document individually. The deviations / fluctuations become then the parameters used to calculate a quantitative value of semantic proximity. 

More information: 
http://scikit-learn.org/stable/modules/feature_extraction.html#text-feature-extraction

Requires scikit-learn
""")    
ap.add_argument('path', metavar='path', nargs='?', default="files",
                    help='path of the folder that contains the files')
args = ap.parse_args()

# Obtains the list of txt documents inside the selected folder

folder = args.path
text_files = []
for f in os.listdir(folder):
    if f.endswith(".txt"):
        text_files.append( folder + "/" + f )

# Reads documents as strings and stores them in an array, the "corpus"

documents = [open(f) for f in text_files]

corpus = []
for doc in documents:
    corpus.append(doc.read())

# CountVectorizer builds the complete list of words
# It does not include the stop words thanks to a sklearn built-in list
# For each document, in counts the appearances of each one of the total words
    
cvectorizer = CountVectorizer(min_df=1, stop_words='english')
X = cvectorizer.fit_transform(corpus)
names = cvectorizer.get_feature_names()

print ""
print "//// 1 / 3 ////////////////////////////////////////////////////////////////////"    
print ""

print "FEATURE EXTRACTION: MOST FREQUENT WORDS"

# Displays the first 5 most frequent words for each text

for i in range(0, len(text_files)):     
    print ""
    print basename(text_files[i])
    
    words = []  
    c = 0
    for i in X[i].toarray()[0]:
        words.append( (i, names[c]))
        c = c + 1    
    
    topwords = ""    
    for number, word in sorted(words, reverse=True)[:5]:
        topwords += "{1}({0})".format(number, word) + " "
    print "-- " + topwords

print ""
print "//// 2 / 3 ////////////////////////////////////////////////////////////////////"      
print ""

print "CALCULATED SIMILARITY VALUE BETWEEN DOCUMENTS"

# Outputs the calculated proximity values between documents

# Performs the Tf-idf term weighting and
# calculates the proximity between documents

transformer = TfidfTransformer()
tfidf = transformer.fit_transform(X)

# ps will be the matrix containing the 
# document-to-document proximity measure:

ps = tfidf * tfidf.T
ps = ps.toarray();

for i in range(0, len(text_files)):
    print ""
    print basename(text_files[i])
    items = []
    for j in range(0, len(text_files)):
        items.append( (ps[i,j],basename(text_files[j])) ) 
    for score, name in sorted(items,reverse=True)[1:]:
        print "-- {0:0.03f} {1}".format(score,name)
        
print ""
print "//// 3 / 3 ////////////////////////////////////////////////////////////////////"      
print ""

print "RECOMMENDED READINGS FLOW"

# The same as before, but in a "Suggested or Recommended Products"
# way of displaying information

for i in range(0, len(text_files)):
    print ""
    print "If you enjoyed reading {0}, you might like also:".format(basename(text_files[i]))
    items = []
    for j in range(0, len(text_files)):
        items.append( (ps[i,j],basename(text_files[j])) ) 
    for score, name in sorted(items,reverse=True)[1:3]:
        print "-- {0}".format(name)
    


