# README #

This is a script that estimates the similatity between documents that are taken two by two from a larger collection. The algorithm calculates the average frequencies of the words used within the collection of all the documents, and compares these values with the local frequencies of the words within each document individually. The deviations / fluctuations become then the parameters used to calculate a quantitative value of semantic proximity. 

More information: 
http://scikit-learn.org/stable/modules/feature_extraction.html#text-feature-extraction

It is written in python and requires [scikit-learn](http://scikit-learn.org/stable/)

It was developed during the [Machine Research](http://machineresearch.wordpress.com/) workshop.

## Usage ##

> #> python tf-idf-document-similarities _path-of-the-folder_

Where _path_of_the_folder_ is the directory where the set of txt files to be compared are stored. If not specified, it uses the default location, _files_. This folder contains a sample set of documents, the initial papers submitted to the [workshop](http://machineresearch.wordpress.com/) by the participants.

## Output ##

Currently, the script outputs the five top feature words for each paper, the similarity value of each document with the rest of the papers, and finally a "Continue reading / Suggested papers" way of displaying this information.

With the default set of files,  for instance, a fragment of the output looks like this:


    //// 1 / 3 ////////////////////////////////////////////////////////////////////
    
    FEATURE EXTRACTION: MOST FREQUENT WORDS

    The_Signification_Communication_Question_Some_Initial_Remarks.txt
    -- language(14) communication(14) reality(11) thought(10) medium(10) 

    //// 2 / 3 ////////////////////////////////////////////////////////////////////
    
    CALCULATED SIMILARITY VALUE BETWEEN DOCUMENTS

    The_Signification_Communication_Question_Some_Initial_Remarks.txt
    -- 0.164 The_Cultural_Politics_of_Information_and_of_Debt.txt
    -- 0.134 Testing_Texting_South_a_Political_Fiction.txt
    -- 0.115 Machine_Listening.txt
    -- 0.092 Pattern-Recognition-across-Bodies-and-Machines-by-Anarchival-Means.txt
    -- 0.085 The_Stupid_Network_that_we_Know_and_Love.txt
    -- 0.084 Elusive_Borders_Aesthetic_Perpectualization_The_Space_Time_of_Metadata.txt
    -- 0.084 Participation_in_Infrastructures.txt
    -- 0.082 Relearn_to_Read_Speed_Readers.txt
    -- 0.073 An_Ethnography_of_Error.txt
    -- 0.071 Machine_Pedagogies.txt
    -- 0.071 Resolution_Theory.txt
    -- 0.066 Unmaking_Screens_a_Genealogy_of_the_Mineral_Vision.txt
    -- 0.048 From_Page_Rank_to_Rankbrain.txt
    -- 0.033 Computing_War_Narratives_The_Hamlet_Evaluation_System_in_Vietman.txt

    //// 3 / 3 ////////////////////////////////////////////////////////////////////
    
    RECOMMENDED READINGS FLOW

    "If you enjoyed reading The_Signification_Communication_Question_Some_Initial_Remarks.txt, you might like also:
    -- The_Cultural_Politics_of_Information_and_of_Debt.txt
    -- Testing_Texting_South_a_Political_Fiction.txt"



