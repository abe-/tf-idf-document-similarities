this post will be an introduction or proposal for resolution theory.

the meaning of the word resolution depends greatly on the context in which it is used. the oxford dictionary for instance, differentiates between an ordinary and a formal usage of the word, while listing definitions from within the discourses of music, medicine, chemistry, physics and optics. what is striking about this list is that at first glance some of these definitions read contradictory; while in chemistry resolution may mean the breaking down of a complex issue or material into smaller pieces, mathematics uses the term resolution to signify the answer that solves a particular problem. when resolution is used from within a more formal, legislative or deliberative assembly, it usually refers to a proposal that requires a vote. in this case, resolution means the deciding of a firm, formal solution and is sometimes used in conjunction with the term motion, as in ‘to accept a motion or formal proposal, in order to resolve a dispute’.[1]

while the formal usage of the term resolution as a final solution seems to oppose the older definitions of resolution, which signify the act of breaking down, etymologically, these different meanings originate from the same latin root. in order to come to terms with the many different uses of the word resolution, and to avert a sense of inceptive ambiguity, i will start this chapter with a short etymology of the word resolution. after doing so, i will describe what resolution can mean when the term is used within a technological context, by deconstructing the meaning of the term used in conjunction with screen, as ’screen resolution’.[2]

an etymology of resolution

the word “resolution” consists of two parts: re-, which stems from the latin prefix meaning again or back, and solution, which can be traced back to the latin action noun solutionem. resolution is thus etymologically rooted in resolutionem, a conjugation that stems from resolvere, meaning “to loosen, release, untie, solve or dissolve” and that suggests a separation or disentanglement of one thing from something that it is tied up with, or “the process of reducing things into simpler forms”.[3]

the oxford dictionary places the origin of resolution in late middle english (but also references the latin word resolvere), while douglas harper describes a kinship with the 14th century french word solucion which translates to division, dissolving or explanation. harper also writes that around the 1520s resolution was used to “determine or to decide upon something” by “breaking (something) into parts to arrive at a truth or to make a final determination”. 

resolving, in terms of "a solving" (as of mathematical problems) was only first recorded in the 1540s, as was its usage in meaning the "power of holding firmly" (resolute). this is where, according to douglas harper, the term to “pass a resolution” stems from (1580s). resolution in terms of a "decision or expression of a meeting" is dated at around 1600, while a resolution made around new year, which generally refers to the specific intention to better oneself, was only first recorded around the 1780s. finally, harper dates the first recording of resolution as referring to the "effect of an optical instrument” back to the 1860s.

the oxford dictionary ends its list of definitions of resolution with: “5) the smallest interval measurable by a telescope or scientific instrument; the resolving power. 5.1) the degree of detail visible in a photographic or television image.” these definitions of the term resolution are the closest to a definition of resolution within the realm of digital technologies. however, they do not shed a light on the use of the term resolution and the diverse actors involved in the action of setting a resolution within a digital realm; in fact, these terms seem to rather conflate the actual processes and dimensions at stake when describing the action of resolutions setting within (digital) technologies.

1) a firm decision to do or not do something, 1.1) a formal expression of opinion or intention agreed on by a legislative body or other formal meeting, typically after taking a vote 2) the quality of being determined or resolute 3) the action of solving a problem in a contentious matter 3.1) music: the passing of a discord into a concord during the course of changing harmony: 3.2) medicine: the disappearance of a symptom or condition 4) chemistry: the process of reducing or separating something into constituent parts or components 4.1) physics: the replacing of a single force or other vector quantity by two or more jointly equivalent to it. 5) the smallest interval measurable by a telescope or scientific instrument; the resolving power. 5.1) the degree of detail visible in a photographic or television image. 

- oxford dictionary of english. edited by angus stevenson. third edition, oxford university press. 2010. p. 1512.
        
dictionary of problem words and expressions. harry shaw. 1975.

in 2013 me and ‘my’ band knalpot were invited to play at the night of the unexpected in moscow, as representatives of the “nederland-rusland jaar”; a year that celebrates international relations between russia and the netherlands. it was an official, bureaucratically organized two-evening event. the projector pointed at a temporary wall; a huge, professionally suspended black veil that divided a space filled with equipment in the back from the main stage. i joked a bit about the blackness of screen to the technician, who seemed stoic when it came to both my search for a white screen, and my jokes about analogue grayscale. at that moment i was not sure about the reality of the situation. i remember the following conversation, with the producer of the event:

< when will we hang the screen?
> this is the screen
< but, i think we need a white screen, not a black screen.
> this is the best technology available in moscow.
< but, the screen is black. black screens absorb light, instead of reflect it - this will make the projection less intense… if visible at all..
> it is the best technology in moscow.
you did not specify you need a white screen on your rider.
< but…
[i am condensing this part of the conversation a bit, but this took a bit of going back and forth between arguments of ‘best technology’ and the behavior and qualities of light]
< ok, we can test it… but i sincerely doubt this will look any good.
can i have the rca cable i requested on my rider?
> [lady points at hdmi cable and analogue to digital converter] we have this for you, it is better quality.
< oh. that is no good. i requested rca. i also need a sound feed from the stage to my synthesizer 

because i play with a component of analogue video synthesis and sound transcoding.
… i would like to send my output unconverted, straight from my synthesizer to the projector…
there was a big disconnect between the depth of the resolved image on the wall and the images resolved on my check monitor not just in terms of esthetics governed by encoding, but also in power, time and aspect ratio. it was then that i first considered the difference between the ‘thicknesses’ of both my check-monitor and the projection, the screens, and the need for a re-articulation of the term ‘resolution’, in order to describe these factors within this expanded definition of resolutions.

the meaning of the word resolution depends greatly on the context in which it is used. the oxford dictionary for instance, differentiates between an ordinary and a formal usage of the word, while listing definitions from within the discourses of music, medicine, chemistry, physics and optics. what is striking about this list is that at first glance some of these definitions read contradictory; while in chemistry resolution may mean the breaking down of a complex issue or material into smaller pieces, mathematics uses the term resolution to signify the answer that solves a particular problem. when resolution is used from within a more formal, legislative or deliberative assembly, it usually refers to a proposal that requires a vote. in this case, resolution means the deciding of a firm, formal solution and is sometimes used in conjunction with the term motion, as in ‘to accept a motion or formal proposal, in order to resolve a dispute’.[1]

while the formal usage of the term resolution as a final solution seems to oppose the older definitions of resolution, which signify the act of breaking down, etymologically, these different meanings originate from the same latin root. in order to come to terms with the many different uses of the word resolution, and to avert a sense of inceptive ambiguity, i will start this chapter with a short etymology of the word resolution. after doing so, i will describe what resolution can mean when the term is used within a technological context, by deconstructing the meaning of the term used in conjunction with screen, as ’screen resolution’.[2]

an etymology of resolution

the word “resolution” consists of two parts: re-, which stems from the latin prefix meaning again or back, and solution, which can be traced back to the latin action noun solutionem. resolution is thus etymologically rooted in resolutionem, a conjugation that stems from resolvere, meaning “to loosen, release, untie, solve or dissolve” and that suggests a separation or disentanglement of one thing from something that it is tied up with, or “the process of reducing things into simpler forms”.[3]

the oxford dictionary places the origin of resolution in late middle english (but also references the latin word resolvere), while douglas harper describes a kinship with the 14th century french word solucion which translates to division, dissolving or explanation. harper also writes that around the 1520s resolution was used to “determine or to decide upon something” by “breaking (something) into parts to arrive at a truth or to make a final determination”.

resolving, in terms of "a solving" (as of mathematical problems) was only first recorded in the 1540s, as was its usage in meaning the "power of holding firmly" (resolute). this is where, according to douglas harper, the term to “pass a resolution” stems from (1580s). resolution in terms of a "decision or expression of a meeting" is dated at around 1600, while a resolution made around new year, which generally refers to the specific intention to better oneself, was only first recorded around the 1780s. finally, harper dates the first recording of resolution as referring to the "effect of an optical instrument” back to the 1860s.

the oxford dictionary ends its list of definitions of resolution with: “5) the smallest interval measurable by a telescope or scientific instrument; the resolving power. 5.1) the degree of detail visible in a photographic or television image.” these definitions of the term resolution are the closest to a definition of resolution within the realm of digital technologies. however, they do not shed a light on the use of the term resolution and the diverse actors involved in the action of setting a resolution within a digital realm; in fact, these terms seem to rather conflate the actual processes and dimensions at stake when describing the action of resolutions setting within (digital) technologies.

generally a ‘resolution’ refers to a determination of functional settings in the technological domain. a resolution is indeed an overall agreed upon settlement (solution). however, a resolution also entails a space of compromise between different actors (objects, materialities, and protocols) in dispute over norms (frame rate, number of pixels etc.). generally, settings either ossify as requirements and de facto standards, or are notated as norms by standardizing organizations such as the international organization for standardization.

at the same time a resolution exists as a space of compromise between different actors (languages, objects, materialities) who dispute their stakes (frame rate, number of pixels and colors, etc.), following rules (protocols) within the ever growing digital territories.
resolutions are non-neutral standard settings that involve political, economical, technological and cultural values and ideologies, embedded in the genealogies and ecologies of our media. in an uncompromisingly fashion, quality (fidelity) speed (governed by efficiency) volume (generally encapsulated in tiny-ness for hardware and big when it comes to data) and profit (economic or ownership) have been responsible for plotting the vectors of progress.
this dogmatic configuration of belief x action has made upgrade culture a great legitimizer of violence, putting its insufficient technological resolutions to rest. while a resolution can thus be understood as a manifold assemblage of common, but contestable standards, it should also be considered in terms of other options; those that are unknown and unseen, obsolete and unsupported.

resolutions inform both machine vision and human ways of perception. they shape the material of everyday life ubiquitously.

as the media landscape becomes more and more compound, or in other words, an heterogenous assemblage in which one technology never functions on its own, its complexities have moved beyond a fold of everyday settings. technological standards have compiled into resolution clusters; media platforms that form resolutions like tablelands, flanked by steep cliffs and precipices looking out over obscure, incremental abysses that seem to harbor a mist of unsupported, obsolete norms.

the platforms of resolution now organize perspective. they are the legitimizers of both inclusion and exclusion of what can not be seen or what should be done, while the fog, the other possibilities become more and more obscure.
it is important to realize that the resolutions platforms are not inherently evil*. they can be impartial. it is important that we unpack these resolutions and note that they are conditioning our perception. a culture that adheres to only one or few platforms of resolutions supports nepotism amongst standards. these clusters actively engage simpleness and mask the issues at stake, savoring stupidity, and are finally bound to escalate into glutinous tech-fascism.

the question is, have we become unable to construct our own resolutions, or have we become oblivious to them?

resolutions work not just as an interface effect* but as hyperopic lens, obfuscating any other possible alternative resolutions from the users screens and media literacy. when we speak about video, we only ever refer to a four cornered moving image. why do we not consider video with more or less corners, timelines, or soundtracks. fonts are monochrome; they do not come with their own textures, gradients or chrominance and luminance mapping. text editors still follow the lay-out of paper; there is hardly any modularity within written word  technologies. even ghosts, the figments of our imagination, have been conditioned to communicate exclusively through analogue forms of noise (the uncanny per default), while aliens communicate through blocks and lines (the more intelligent forms of noise).

we are hiking the resolution platforms comfortably. unknowingly suffering from technological hyperopia, we have lost track of the compromises that are at stake inside our resolutions and are staring at the screens showing us mirage after mirage.

a resolution is the lens through which constituted materialities become signifiers in their own right. they resonate the tonality of the hive mind and constantly transform our technologies into informed material vernaculars.

technology is evolving faster than we, as a culture can come to terms with. this is why determinations such as standards are dangerous; they can preclude the alternative. the radical digital materialist believes in informed materiality*: while every string of data is ambiguously fluid and has the potential to be manipulated into anything, every piece of information functions within /*adhesive/ encoding, contextualization and embedding. different forms of ossification slither into every crevice of private life, while unresolved, ungoverned free space seems to be slipping away. there lies the power of standardization.

we are in need for a re-(re-)distribution of the sensible.

resolution studies is not only about the effects of technological *progress or about the esthetization of the scales of resolution. resolution studies is a studies on how resolution embeds the tonalities of culture, in more than just its technological facets.
resolution studies researches the standards that could have been in place, but are not. as a form of vernacular resistance, based on the concept of providing ambiguous resolutions.

*1) a firm decision to do or not do something, 1.1) a formal expression of opinion or intention agreed on by a legislative body or other formal meeting, typically after taking a vote 2) the quality of being determined or resolute 3) the action of solving a problem in a contentious matter 3.1) music: the passing of a discord into a concord during the course of changing harmony: 3.2) medicine: the disappearance of a symptom or condition 4) chemistry: the process of reducing or separating something into constituent parts or components 4.1) physics: the replacing of a single force or other vector quantity by two or more jointly equivalent to it. 5) the smallest interval measurable by a telescope or scientific instrument; the resolving power. 5.1) the degree of detail visible in a photographic or television image.

*- oxford dictionary of english. edited by angus stevenson. third edition, oxford university press. 2010. p. 1512.

*dictionary of problem words and expressions. harry shaw. 1975.
